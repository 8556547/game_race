Beginscreen::Beginscreen(GameFSM* context): _context(context) {};

void Beginscreen::Handle()
{
	
	std::cout << "MainMenu::MainMenu \n ";
	std::cout << std::endl << std::endl << std::endl << std::endl;
	std::cout << " \t\t\t\t ==============================================================="<< std::endl;
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t World Racing \t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t Formula 1 \t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t ==============================================================="<< std::endl;
	_getch();
	system("cls");
	_context -> setState(new MainSceen(_context));
}